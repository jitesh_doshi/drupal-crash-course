<?php
/**
 * @file
 * Default theme implementation to display a todo list.
 *
 * Available variables:
 * - $title: the title/caption of the todo list.
 * - $items: array of todo objects from the database.
 */
?>
<div class="todo-new-link-top"><?php echo l('Create TODO', 'todo/new'); ?></div>
<table>
  <?php if (!empty($title)): ?>
    <caption><?php echo $title; ?></caption>
  <?php endif; ?>
  <thead>
    <tr>
      <th>Todo</th>
      <th>Edit</th>
      <th>Delete</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($items as $item): ?>
      <tr>
        <td><?php echo $item->title; ?></td>
        <td><?php echo l(t('edit'), "todo/$item->id/edit"); ?></td>
        <td><?php echo l(t('delete'), "todo/$item->id/delete"); ?></td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<div class="todo-new-link-bottom"><?php echo l('Create TODO', 'todo/new'); ?></div>
